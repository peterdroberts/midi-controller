#ifndef _DISPLAY_H
#define _DISPLAY_H
#include "Arduino.h"
#include "ShiftRegister.h"
class Display {
      public:
	Display(ShiftRegister *shift);
	void init(const byte *numbermap, int registernumber);
	void next();
	void last();
	void value(int value);

      private:
	int _value;
	int _registernumber;
	const byte *_numbermap;
	ShiftRegister *_shift;
};

#endif
