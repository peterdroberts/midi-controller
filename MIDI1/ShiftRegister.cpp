#include "ShiftRegister.h"

/**
 * @brief Shift register initialisation
 * @description User must provide numbered latch, clock and data pins and these will be initialised by the arduino HAL
 */
ShiftRegister::ShiftRegister(int latchpin, int clockpin, int datapin) {
	_latchpin = latchpin;
	_clockpin = clockpin;
	_datapin  = datapin;
	pinMode(_latchpin, OUTPUT);
	pinMode(_datapin, OUTPUT);
	pinMode(_clockpin, OUTPUT);
	for (int i = 0; i < SHIFT_REGISTER_COUNT; i++) {
		_writevalue[i] = 0xFF;
	}
}

void ShiftRegister::setPin(int pin, int value, int registernumber) {
	_writevalue[registernumber] = _writevalue[registernumber] & (1 << pin);
}

void ShiftRegister::setRegister(byte value, int registernumber) { _writevalue[registernumber] = value; }

void ShiftRegister::writeAll() {
	if (_datapin != 0 && _clockpin != 0 && _latchpin != 0) {
		digitalWrite(_latchpin, LOW);
		for (int i = 0; i < SHIFT_REGISTER_COUNT; i++) {
			shiftOut(_datapin, _clockpin, MSBFIRST, _writevalue[i]);
		}
		digitalWrite(_latchpin, HIGH);
	}
}
