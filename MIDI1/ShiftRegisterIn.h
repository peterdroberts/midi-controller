/**
 * @brief Control input shift register
 */
#ifndef _SHIFTREGISTERIN_H
#define _SHIFTREGISTERIN_H
#include "Arduino.h"
#define DATA_WIDTH              16
#define PULSE_WIDTH_USEC        5
#define SHIFT_REGISTER_IN_COUNT 2 /**< Number of shift registers */

class ShiftRegisterIn {
      public:
	ShiftRegisterIn(int ploadPin, int clockEnablePin, int dataPin, int clockPin);
	unsigned int readRegister(int registernumber);

      private:
	int _ploadPin;
	int _clockEnablePin;
	int _dataPin;
	int _clockPin;
};

#endif
