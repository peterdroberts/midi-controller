#include "midifader.h"

MIDIfader::MIDIfader(int parameter, int pin) {
	_pin       = pin;
	_parameter = parameter;
	_value     = 500;
}
void MIDIfader::handle() {
	int analogValue;
	int midiValue;
	if (_pin == 0) {

	} else {
		analogValue = analogRead(_pin);
		midiValue   = map(analogValue, 0, 1023, 0, 127);
		if (midiValue != _value) {
			this->controlChange(_parameter, midiValue);
		} else {
		}
		_value = midiValue;
	}
}
