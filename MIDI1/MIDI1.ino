#include "ShiftRegister.h"
#include "ShiftRegisterIn.h"
#include "buttonbank.h"
#include "display.h"
#include "midifader.h"
#include "midinote.h"
#include "octaveswitcher.h"

#define FADER_COUNT                       4
#define OUTPUT_SHIFT_REGISTER_BUTTONS     0
#define OUTPUT_SHIFT_REGISTER_DISPLAY     1
#define INPUT_SHIFT_REGISTER_BUTTONS      1
#define INPUT_SHIFT_REGISTER_OCTAVESWITCH 0
/* Map LED values to numbers shown on 7-segment display */

const byte displaymap[10] = {B00010001, B01110111, B00101001, B00100011, B01000111,
			     B10000011, B10000001, B00110111, B00000001, B0000011};

/* Initialisation for MIDI fader is {parameter to control, analog pin */
MIDIfader faders[FADER_COUNT] = {{0x10, A0}, {0x11, A1}, {0x12, A2}, {0x13, A3}};

ShiftRegister shiftout(8, 12, 11);
ShiftRegisterIn shiftin(7, 5, 4, 6);
Display sevensegment(&shiftout);
OctaveSwitcher octaveswitcher(&sevensegment, &shiftin);
Buttonbank buttons(&shiftout, &shiftin, &octaveswitcher);

void setup() {
	/* Initialise components */
	sevensegment.init(displaymap, OUTPUT_SHIFT_REGISTER_DISPLAY);
	octaveswitcher.init(INPUT_SHIFT_REGISTER_OCTAVESWITCH);
	buttons.init(OUTPUT_SHIFT_REGISTER_BUTTONS, INPUT_SHIFT_REGISTER_BUTTONS);
	Serial.begin(31250);
}

void loop() {
	/* Run handling for each component */
	buttons.handle();
	octaveswitcher.handle();
	for (int i = 0; i < FADER_COUNT; i++) {
		faders[i].handle();
	}
}
