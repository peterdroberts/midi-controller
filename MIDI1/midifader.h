#ifndef _MIDIFADER_H
#define _MIDIFADER_H

#include "Arduino.h"
#include "midicore.h"

class MIDIfader : public MIDIcore {
      public:
	/**
	 * @param parameter Parameter to fade
	 * @param pin Pin number fader is attached to
	 */
	MIDIfader(int parameter, int pin);
	void init(int parameter, int pin);
	void handle();

      private:
	int _pin;
	int _parameter;
	int _value;
	int _counter;
};
#endif
