#include "midicore.h"
MIDIcore::MIDIcore() {

}

/** 
 * @brief Send note on command
 */
void MIDIcore::noteOn(uint8_t pitch, uint8_t velocity) {
  this->_do(0x90, pitch, velocity);
}

/** 
 * @brief Send note off command
 */
void MIDIcore::noteOff(uint8_t pitch, uint8_t velocity) {
  this->_do(0x80, pitch, velocity);
}

/** 
 * @brief Send mode-change command
 */
void MIDIcore::controlChange(uint8_t controlNumber, uint8_t value) {
  this->_do(0xB0, controlNumber, value);
}

/** 
 * @brief Execute a midi command
 * @param cmd command code
 * @param param1 first command byte
 * @param param2 second command byte
 */
void MIDIcore::_do(uint8_t cmd, uint8_t param1, uint8_t param2) {
  Serial.write(cmd);
  Serial.write(param1);
  Serial.write(param2);
}
