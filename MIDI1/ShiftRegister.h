/** @file ShiftRegister.h Module for controlling a shift register */
#ifndef _SHIFTREGISTER_H
#define _SHIFTREGISTER_H
#include "Arduino.h"

#define SHIFT_REGISTER_COUNT 2

class ShiftRegister {
      public:
	ShiftRegister(int latchpin, int clockpin, int datapin);
	void setRegister(byte value, int registernumber);
	void setPin(int pin, int value, int registernumber);
	void writeAll();
	void writePin(int pin, int value, int registernumber);

      private:
	int _clockpin;
	int _datapin;
	int _latchpin;
	int _shift_register_count;
	int _writevalue[SHIFT_REGISTER_COUNT];
};

#endif
