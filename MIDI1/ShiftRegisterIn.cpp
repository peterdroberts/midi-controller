#include "ShiftRegisterIn.h"

/**
 * @brief Shift register initialisation
 * @description User must provide load, clock enable, clock and data pins and these will be initialised by the arduino
 * HAL
 */
ShiftRegisterIn::ShiftRegisterIn(int ploadPin, int clockEnablePin, int dataPin, int clockPin) {
	_ploadPin       = ploadPin;
	_clockEnablePin = clockEnablePin;
	_dataPin        = dataPin;
	_clockPin       = clockPin;
	pinMode(_ploadPin, OUTPUT);
	pinMode(_clockEnablePin, OUTPUT);
	pinMode(_clockPin, OUTPUT);
	pinMode(_dataPin, INPUT);

	digitalWrite(_clockPin, LOW);
	digitalWrite(_ploadPin, HIGH);
}
/**
 * @brief Read the value of a specific register
 */
unsigned int ShiftRegisterIn::readRegister(int registernumber) {
	long bitVal;
	unsigned int bytesVal = 0;
	digitalWrite(_clockEnablePin, HIGH);
	digitalWrite(_ploadPin, LOW);
	delayMicroseconds(PULSE_WIDTH_USEC);
	digitalWrite(_ploadPin, HIGH);
	digitalWrite(_clockEnablePin, LOW);
	for (int i = 0; i < DATA_WIDTH; i++) {
		bitVal = digitalRead(_dataPin);

		/* Set the corresponding bit in bytesVal.
		 */
		bytesVal |= (bitVal << ((DATA_WIDTH - 1) - i));

		/* Pulse the Clock (rising edge shifts the next bit).
		 */
		digitalWrite(_clockPin, HIGH);
		delayMicroseconds(PULSE_WIDTH_USEC);
		digitalWrite(_clockPin, LOW);
	}
	bytesVal = (bytesVal >> (registernumber * 8));
	return (bytesVal);
}
