#ifndef _BUTTONBANK_H
#define _BUTTONBANK_H
#include "Arduino.h"
#import "midicore.h"
#include "Bounce2.h"
#include "ShiftRegister.h"
#include "ShiftRegisterIn.h"
#include "octaveswitcher.h"

#define BUTTON_BANK_SIZE 8

class Buttonbank : public MIDIcore {
  public:
    Buttonbank(ShiftRegister *shiftout, ShiftRegisterIn *shiftin, OctaveSwitcher *octave);
    void init(int shiftoutnumber, int shiftinnumber);
    void handle();
  private:
    int _shiftinnumber;
    int _shiftoutnumber;
    ShiftRegister *_shiftout;
    ShiftRegisterIn *_shiftin;
    OctaveSwitcher *_octave;
    unsigned int _buttonvalues;
    bool _buttons[BUTTON_BANK_SIZE];
    int _buttonvals[BUTTON_BANK_SIZE];
};
#endif
