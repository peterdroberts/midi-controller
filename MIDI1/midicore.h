#ifndef _MIDICORE_H
#define _MIDICORE_H
#import "Arduino.h"
class MIDIcore {
  public:
    MIDIcore();
    void noteOn(uint8_t pitch, uint8_t velocity);
    void noteOff(uint8_t pitch, uint8_t velocity);
    void controlChange(uint8_t controlNumber, uint8_t value);
  private:
    void _do(uint8_t cmd, uint8_t param1, uint8_t param2);
};
#endif

