#ifndef _OCTAVESWITCHER_H
#define _OCTAVESWITCHER_H
#include "Arduino.h"
#include "midicore.h"
#include "display.h"
#include "ShiftRegister.h"
#include "ShiftRegisterIn.h"
class OctaveSwitcher : public MIDIcore {
  public:
    OctaveSwitcher(Display *sevensegmentdisplay, ShiftRegisterIn *shiftin);
    void init(int shiftinnumber);
    void handle();
    int value();
  private:
    bool _wasPressedUp;
    bool _wasPressedDown;
    int _value;
    int _shiftinnumber;
    int _shiftoutnumber;
    Display *_sevensegmentdisplay;
    ShiftRegisterIn *_shiftin;
};
#endif
