#include "octaveswitcher.h"

/* The octave switcher is a seven segment display displaying the octave and two
 * buttons to move the octave up or down */

OctaveSwitcher::OctaveSwitcher(Display *sevensegmentdisplay, ShiftRegisterIn *shiftin) {
	_sevensegmentdisplay = sevensegmentdisplay;
	_shiftin             = shiftin;
	_wasPressedUp        = LOW;
	_wasPressedDown      = LOW;
	_value               = 5;
}

void OctaveSwitcher::init(int shiftinnumber) { _shiftinnumber = shiftinnumber; }
int OctaveSwitcher::value() { return _value; }
void OctaveSwitcher::handle() {
	unsigned int buttonvals = _shiftin->readRegister(_shiftinnumber);
	/* Find out if one of the octave switcher buttons has been pressed and switch value accordingly */
	if (_wasPressedUp || _wasPressedDown) {
		if (buttonvals & 0x01 == HIGH) {
			_wasPressedUp = HIGH;
		} else if ((buttonvals >> 1) & 0x01 == HIGH) {
			_wasPressedDown = HIGH;
		} else {
			_wasPressedDown = LOW;
			_wasPressedUp   = LOW;
		}
	} else if (buttonvals & 0x01 == HIGH) {
		_wasPressedUp = HIGH;
		_value++;
	} else if ((buttonvals >> 1) & 0x01 == HIGH) {
		_wasPressedDown = HIGH;
		_value--;
	}
	/* Cap octave maximum 9 (7 segment display) */
	if (_value > 9) {
		_value = 9;
	}
	if (_value < 0) {
		_value = 0;
	}
	/* Write value to 7 segment display */
	_sevensegmentdisplay->value(_value);
}
