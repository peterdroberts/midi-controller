#import "display.h"

Display::Display(ShiftRegister *shift) { _shift = shift; }
void Display::next() {
	_value++;
	if (_value > 9) {
		_value = 0;
	}
	this->value(_value);
}
void Display::last() {
	_value--;
	if (_value < 0) {
		_value = 9;
	}
	this->value(_value);
}
void Display::value(int value) {
	if (value >= 0 && value <= 9) {
		_shift->setRegister(_numbermap[value], _registernumber);
		_shift->writeAll();
	}
}

void Display::init(const byte *numbermap, int registernumber) {
	_numbermap      = numbermap;
	_registernumber = registernumber;
	_value          = 0;
}
