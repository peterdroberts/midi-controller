#ifndef _MIDINOTE_H
#define _MIDINOTE_H
#include "Arduino.h"
#include "Bounce2.h"
#include "midicore.h"
class MIDInote : public MIDIcore {
      public:
	MIDInote();
	void init(int pin, int pitch);
	void handle();

      private:
	bool _wasPressed;
	int _pressCounter = 0;
	int _pin          = 0;
	int _pitch        = 0;
	Bounce _debouncer;
};
#endif
