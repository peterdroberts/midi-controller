#import "buttonbank.h"

/* The button bank is a series of light-up buttons */
Buttonbank::Buttonbank(ShiftRegister *shiftout, ShiftRegisterIn *shiftin, OctaveSwitcher *octave) {
	_shiftout = shiftout;
	_shiftin  = shiftin;
	_octave   = octave; // The button bank takes an octave switcher instance in order to determine the output notes
}

void Buttonbank::init(int shiftoutnumber, int shiftinnumber) {
	_shiftinnumber  = shiftinnumber;
	_shiftoutnumber = shiftoutnumber;
}

/**
 * @brief Do all operations for the button bank
 */
void Buttonbank::handle() {
	bool buttons[BUTTON_BANK_SIZE];

	_buttonvalues = _shiftin->readRegister(_shiftinnumber);
	_shiftout->setRegister(_buttonvalues, _shiftoutnumber);

	_shiftout->writeAll();
	for (int i = 0; i < BUTTON_BANK_SIZE; i++) {
		buttons[i] = (_buttonvalues >> i) & 1;
		if (_buttons[i] != buttons[i]) {
			if (buttons[i] == 0) {
				this->noteOff(_buttonvals[i], 50);
			} else {
				_buttonvals[i] = 12 * _octave->value() + i;
				this->noteOn(_buttonvals[i], 50);
			}
		}
		_buttons[i] = buttons[i];
	}
}
