#include "midinote.h"

MIDInote::MIDInote() {}
void MIDInote::init(int pitch, int pin) {
	_debouncer = Bounce();
	_pin       = pin;
	_pitch     = pitch;
	pinMode(_pin, INPUT_PULLUP);
	_debouncer.attach(_pin);
	_debouncer.interval(10);
}
void MIDInote::handle() {
	_debouncer.update();

	int pressed = !_debouncer.read();
	if (_pin == 0 || _pitch == 0) {

	} else {
		if (!_wasPressed && pressed) {
			this->noteOn(_pitch, 0x45);
			_wasPressed = true;
			_pressCounter == 0;
		} else if (_wasPressed && !pressed) {
			this->noteOff(_pitch, 0x45);
			_wasPressed = false;
			_pressCounter == 0;
		} else {
			_pressCounter++;
		}
	}
}
